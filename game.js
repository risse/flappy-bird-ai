// Create our 'main' state that will contain the game
var mainState = {
    rockString: "42312441243124314234123413242423142432143213",
    rockIndex: 0,
    generation: 0,
    preload: function() { 
        // Load the sprites
        game.load.image('rockBottom', 'graphics/PNG/rockGrass2x.png');
        game.load.image('rockTop', 'graphics/PNG/rockGrassDown2x.png');
        game.load.image('ground', 'graphics/PNG/groundGrass.png');
        game.load.spritesheet('plane', 'graphics/Spritesheet/planes.png', 88, 73);
    },

    create: function() { 
        // Change the background color of the game to blue
        game.stage.backgroundColor = '#71c5cf';

        // Set the physics system
        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.planes = [];

        this.rockIndex = 0;

        var planeAnimations = [
            {
                color: "yellow",
                animation: [4, 6, 8]
            },{
                color: "blue",
                animation: [0, 2, 10]
            },{
                color: "green",
                animation: [3, 5, 7]
            },{
                color: "red",
                animation: [1, 9, 12]
            }            
        ];

        for(i = 0; i < populationSize; i++) {
            this.planes.push(game.add.sprite(100, 245, 'plane'));

            game.physics.arcade.enable(this.planes[i]);

            this.planes[i].score = 0;

            this.planes[i].body.gravity.y = 1000; 

            this.planes[i].planeColor = planeAnimations[Math.floor(Math.random() * planeAnimations.length)];

            var planeFly = this.planes[i].animations.add('planeFly', this.planes[i].planeColor.animation);

            this.planes[i].animations.play('planeFly', 6, true);

        }

        // Create an empty group
        this.rocks = game.add.group(); 

        this.planesAliveStyle = { font: "30px Arial", fill: "#fff", align: "center" };

        this.planesAliveText = game.add.text(10, 10, "Planes alive: "+populationSize, this.planesAliveStyle);

        this.timer = game.time.events.loop(100, this.updatePlanes, this);

        this.addRocks();

        this.timer = game.time.events.loop(2500, this.addRocks, this); 

        var ground = game.add.sprite(0, gameHeight - game.cache.getImage('ground').height, 'ground');

        // Enable physics on the rock 
        game.physics.arcade.enable(ground);

        ground.body.velocity.x = -200;

        for(var i = 1; i < 5; i++) {

            ground = game.add.sprite((game.cache.getImage('ground').width * i), gameHeight - game.cache.getImage('ground').height, 'ground');

            // Enable physics on the rock 
            game.physics.arcade.enable(ground);

            ground.body.velocity.x = -200;

        }

    },

    update: function() {

    },

    updatePlanes: function() {

        var self = this;

        var planeIsOnScreen = false;

        var planesAlive = populationSize;

        var lastRock = self.rocks.getBottom();

        self.planes.forEach(function(plane, index) {

            if(plane.alive) {

                var toJump = Ai.getOutput(index, [
                    plane.body.velocity.y, 
                    lastRock.x, 
                    (lastRock.hole - plane.y)
                ]);

                if(toJump > 0.50) {
                    self.jump(plane);
                }

                self.game.physics.arcade.overlap(plane, self.rocks, self.hitRock, null, self);

                if (plane.y > 0 && plane.y < gameHeight) {
                    
                    // Get score based on how close they are to the hole vertically

                    plane.score += (gameHeight - (Math.abs(lastRock.hole - plane.y)));

                    planeIsOnScreen = true;
                } else {
                    plane.alive = false;
                }

            } else {

                planesAlive -= 1;

            }

        });

        if(!planeIsOnScreen) {

            // Prevent new rocks from appearing
            game.time.events.remove(this.timer);

            game.end();

        }            

        this.planesAliveText.setText("Planes alive: "+planesAlive);

    },
    // Make the plane jump 
    jump: function(plane) {

        if (plane.alive == false)
            return;  
        // Add a vertical velocity to the plane
        plane.body.velocity.y = -300;

    },
    addOneRock: function(x, y, hole) {
        // Create a rock at the position x and y
        var rock = game.add.sprite(x, y, 'rock');

        // Add the rock to our previously created group
        this.rocks.add(rock);

        // Add the hole center position to every rock
        rock.hole = hole;

        // Enable physics on the rock 
        game.physics.arcade.enable(rock);

        // Add velocity to the rock to make it move left
        rock.body.velocity.x = -200; 

        // Automatically kill the rock when it's no longer visible 
        rock.checkWorldBounds = true;
        rock.outOfBoundsKill = true;
    },
    addRowOfRocks: function() {

        var hole = parseInt(this.rockString[this.rockIndex]);

        var holeCenter = Math.round((hole * 60 + 10) + ((hole + 1) * 60 + 10) / 2); 

        // Add the rocks 
        // With one big hole at position 'hole' and 'hole + 1'
        for (var i = 0; i < 11; i++)
            if (i != hole && i != hole + 1) 
                this.addOneRock(gameWidth, i * 60 + 10, holeCenter);

        this.rockIndex += 1;
    },
    hitRock: function(plane) {
        // If the plane has already hit a rock, do nothing
        // It means the plane is already falling off the screen
        if (plane.alive == false)
            return;

        plane.kill();

    },
    addRocks: function() {

        var hole = parseInt(this.rockString[this.rockIndex]);

        var holeCenter = Math.round((hole * 60 + 10) + ((hole + 1) * 60 + 10) / 2); 

        var rockBottom = game.add.sprite(gameWidth, holeCenter + 80, 'rockBottom');

        // Add the rock to our previously created group
        this.rocks.add(rockBottom);

        // Add the hole center position to every rock
        rockBottom.hole = holeCenter;

        // Enable physics on the rock 
        game.physics.arcade.enable(rockBottom);

        // Add velocity to the rock to make it move left
        rockBottom.body.velocity.x = -200; 

        // Automatically kill the rock when it's no longer visible 
        rockBottom.checkWorldBounds = true;
        rockBottom.outOfBoundsKill = true;

        var rockTop = game.add.sprite(gameWidth, (holeCenter - 80) - game.cache.getImage('rockTop').height, 'rockTop');

        // Add the rock to our previously created group
        this.rocks.add(rockTop);

        // Add the hole center position to every rock
        rockTop.hole = holeCenter;

        // Enable physics on the rock 
        game.physics.arcade.enable(rockTop);

        // Add velocity to the rock to make it move left
        rockTop.body.velocity.x = -200; 

        // Automatically kill the rock when it's no longer visible 
        rockTop.checkWorldBounds = true;
        rockTop.outOfBoundsKill = true;

        this.rockIndex += 1;

    }
};