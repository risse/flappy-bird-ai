# Flappy Bird AI

Runs a genetic algorithm simulation of AI playing Flappy Bird game.

Uses [Brainwave](https://github.com/zefman/Brainwave) AI library and [Phaser](http://phaser.io) game library

## Installation

You will need [Yarn](https://yarnpkg.com/en/) for installation.

If you want to use NVM, you can run `nvm use` before `yarn` commands.

```
yarn
yarn serve
```

The site is then running on `http://localhost:8000`