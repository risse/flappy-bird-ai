var Ai = {

  networks: [],

  generation: 1,

  genetics: {},

  init: function() {

    // Populate the networks array
    for (var i = 0; i < populationSize; i++) {
      this.networks.push(new Brainwave.Network(3, 1, 2, 20));
    }

    // Next we need to create the Genetics object that will evolve the networks for us
    this.genetics = new Brainwave.Genetics(populationSize, this.networks[0].getNumWeights(), {
        // Probability genomes will exchange some weights
        crossoverRate  : 0.3,
        // The chance that some weights will mutate - 0.05 to 0.3 is good
        mutationRate   : 0.05,
        // The maximum amount a weight can mutate by
        maxPertubation : 0.2,
        // The top n best performing genomes to be copied directly
        // into the next generation. numElite * numEliteCopies MUST BE EVEN
        numElite       : 1,
        // The number of copies of each elite to be placed into the
        // next population
        numEliteCopies : 20
    });

  },

  getOutput: function(index, inputs) {

   return this.networks[index].run(inputs)[0];

  },

  evolve(planes, callback) {

    var fitness = 0;

    var bestNetwork = {
      fitness: 0
    };

    for (var k = 0; k < populationSize; k++) {

      fitness = planes[k].score;

      // Now we need to update the genetics with this fitness
      this.genetics.population[k].fitness = fitness;

      if(fitness > bestNetwork.fitness) {

        bestNetwork = this.networks[k];
        bestNetwork.fitness = fitness;
        bestNetwork.color = planes[k].planeColor.color;

      }

    }

    updateHighScores(bestNetwork, this.generation, planes);

    // After you have decided on a fitness for each network, we can use the genetics
    // object to evolve them based on the results
    this.genetics.epoch(this.genetics.population);

    // Then we just need to import the new weights into the networks and repeat again and again
    for (var n = 0; n < populationSize; n++) {
      this.networks[n].importWeights(this.genetics.population[n].weights);
    }

    this.generation += 1;

    callback();

  }

};